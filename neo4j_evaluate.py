#!/usr/bin/env python

import argparse
from os.path import isfile
import json
from typing import List, Dict, Union, Any, Set, Iterable
import matplotlib.pyplot as plt
from numpy import ndarray
from scipy.stats import binom, hypergeom, chi2_contingency
import numpy as np
import pprint
from py2neo import *

# !/usr/bin/env python


'''
Command to run the script from the root of the directory:  
./neo4j_evaluate.py -q set11.txt -t GOTerm --species 511145 -c -v
'''


# SCRIPT PARAMETERS
parser = argparse.ArgumentParser(description='Search enriched terms/categories in the provided (gene) set')
parser.add_argument('-q', '--query', required=True, help='Query set.')  # Set attributed for the project
parser.add_argument('-t', '--sets', required=True, help='Target sets node type.') # GOTerm
parser.add_argument('-s', '--species', required=True, type=int, help='Taxon id.') # specie choosen is 511145
parser.add_argument('-a', '--alpha', required=False, type=float, default=0.05, help='Significance threshold.')
parser.add_argument('-c', '--adjust', required=False, action="store_true", help='Adjust for multiple testing (FDR).')
parser.add_argument('-m', '--measure', required=False, default='binomial',
                    help='Dissimilarity index: binomial (default), hypergeometric, chi2 or coverage. ')
parser.add_argument('-l', '--limit', required=False, type=int, default=0, help='Maximum number of results to report.')
parser.add_argument('-v', '--verbose', required=False, action="store_true", help='Talk a lot.') # Argument allowing the screening of the results
param = parser.parse_args()


######################## FUNCTIONS NEEDED #####################


#### 1. Calcul of the p-values with the chosen measure
def statistic_test(measure , query, elements, population_size): #arguments needed : measure method, query size, elements, population size
    query_size = len(query)
    common_elements = elements.intersection(query)

    if measure == 'binomial':
            # discrete probability distribution that describes the probability of k successes in n draws, with replacement
            # binom.cdf(>=success, attempts, proba)
        pvalue = binom.cdf(query_size - len(common_elements), query_size,
                           1 - float(len(elements)) / population_size) #scipy function

    elif measure == 'coverage':
        pvalue = 1 - ((len(common_elements) / query_size) * (len(common_elements) / len(elements)))

    elif measure == 'chi2': #chi-square goodness of fit test determines if sample data matches a population
        contingence = np.array([[len(common_elements), (query_size - len(common_elements))],
                                [(len(elements) - len(common_elements)),
                                 (population_size - query_size - len(elements) + len(common_elements))]])
        if (len(common_elements) != 0):
            pvalue = chi2_contingency(contingence)[1] #scipy function

    elif measure == 'hypergeometric':
                # discrete probability distribution that describes the probability of k successes in n draws, without replacement
        pvalue = hypergeom.sf(len(common_elements) - 1, population_size, len(elements), query_size) #scipy

    return pvalue

### 2. Pvalues calcul in order to plot the evolution of each method
def CalculPval(measure, Q, T, C):
    #Options : measure method, Q, T, C
    #a pvalue will be calculated for the parameters choosen
    G = 100
    if measure == 'binomial' :
        pval = binom.cdf( Q - C, Q, 1 - T/G)
    elif measure == 'hypergeometric' :
        pval = hypergeom.sf(C - 1, G, T, Q)
    elif measure == 'chi2' :
        contingence = np.array([[C, (Q - C)],
                                [(T - C), (G - Q - T) + C]])
        pval = chi2_contingency(contingence)[1]
    elif measure == 'coverage' :
        pval = 1 - (C / Q) * (C / T)

    return pval

### 3. Vector creation in order to plot the evolution of each method
def CreationVecs(measure):
    #allowing pvalue calcul for different values of Q, T, C
    vecQ = []
    vecT = []
    vecC = []
    vecpval = []
    for Q in range(1, 30) :
        for T in range (1, 30) :
            for C in range (1, min(Q, T)) :
                vecQ.append(Q)
                vecT.append(T)
                vecC.append(C)
                vecpval.append(CalculPval(measure, Q, T, C))
    return (vecQ, vecT, vecC, vecpval)

### 4. Visualisation of the evolution throught 3D plot projections
def Visualisation(vecQ, vecT, vecC, vecpval):
    fig = plt.figure()
    ax = fig.gca(projection = '3d')
    surface = ax.scatter(vecQ, vecT, vecC, zdir = 'z', s = 30, c = vecpval, depthshade = True)
    fig.colorbar(surface)
    title = ax.set_title("P-value values evolution according different values of Q, T and C ")
    title.set_y(1.01)
    plt.show()



# LOAD QUERY
text = param.query
query = set()
if isfile(text):
    with open(text) as f:
        content = ' '.join(f.read().split('\n')).split()
        query |= set(content)
else:  # parse string
    query |= set(text.split())

if param.verbose:
    print(f'query set: {query}')

# CONNECT TO Neo4J
neo = Graph("http://localhost:7474/", auth=("neo4j", "bioinfo"))
nodes = NodeMatcher(neo)

# COMPUTE POPULATION SIZE
population_size = nodes.match('Gene', taxon_id=param.species).count()
if param.verbose:
    print("population_size: ", population_size)

# RETRIEVE TARGET SETS FOR QUERYc
path = '[:is_a|part_of|annotates*]' if param.sets == 'GOTerm' else ''
cypher = f"MATCH (t:{param.sets})-{path}->(n:Gene {{taxon_id:{param.species} }}) WHERE n.id IN ['" + "', '".join(
    query) + "'] RETURN DISTINCT t"
if param.verbose:
    print(cypher)

sets = neo.run(cypher)


# EVALUATE SETS
# results: List[Dict[str, Union[Union[ndarray, Iterable, int, float, Set[Any]], Any]]] = []
results = []
query_size = len(query)
for s in sets:  # _ids:
    sid = s['t']['id']
    # RETRIEVE TARGET SET ELEMENTS
    path = '[:is_a|part_of|annotates*]' if param.sets == 'GOTerm' else ''
    cypher = "MATCH (t:{})-{}->(n:Gene {{taxon_id:{} }}) WHERE t.id='{}' RETURN n.id".format(param.sets, path,
                                                                                             param.species, sid)
    if param.verbose:
        print(cypher)
    table = neo.run(cypher).to_table()
    # elements = set([ table[i][0] for i in range(len(table)) ])
    # elements = set( list( zip(*table) )[0] )
    elements = set(map(lambda x: x[0], table))
    # print("elements:", elements)
    common_elements = elements.intersection(query)



    if len(common_elements) < 2:
        next
    else:
        pvalue = statistic_test(param.measure, query, elements, population_size)

    r = {'id': sid, 'desc': s['t']['desc'], 'common.n': len(common_elements), 'target.n': len(elements),
         "p-value": pvalue, "elements.target": elements, 'elements.common': common_elements}
    results.append(r)

    if param.verbose:
        print(results)

# PRINT SIGNIFICANT RESULTS
results.sort(key=lambda an_item: an_item['p-value'])
i = 1
for r in results:
    # FDR
    if param.adjust and r['p-value'] > param.alpha * i / len(results): break
    # limited output
    if param.limit > 0 and i > param.limit:
        break
    # alpha threshold
    elif r['p-value'] > param.alpha:
        break
    # OUTPUT
    pval = "{:.4f}".format(r['p-value']) if r['p-value'] > 0.01 else "{:.2e}".format(r['p-value'])
    print("{}\t{}\t{}/{}\t{}\t{}".format(r['id'], pval, r['common.n'], r['target.n'], r['desc'],
                                         ', '.join(r['elements.common'])))
    i += 1


# CHOOSE THE BEST MESURE : VISUALISATION

#Q_binomial, T_binomial, C_binomial, pval_binomial = CreationVecs('binomial')
#Q_hypergeometric, T_hypergeometric, C_hypergeometric, pval_hypergeometric = CreationVecs('hypergeometric')
#Q_chi2, T_chi2, C_chi2, pval_chi2 = CreationVecs('chi2')
#Q_coverage, T_coverage, C_coverage, pval_coverage = CreationVecs('coverage')

#Visualisation(Q_binomial, T_binomial, C_binomial, pval_binomial)
#Visualisation(Q_hypergeometric, T_hypergeometric, C_hypergeometric, pval_hypergeometric)
#Visualisation(Q_chi2, T_chi2, C_chi2, pval_chi2)
#Visualisation(Q_coverage, T_coverage, C_coverage, pval_coverage)






